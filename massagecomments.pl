#!/usr/bin/perl
# $Id: massagecomments.pl,v 1.4 2000/11/04 20:08:31 laine Exp $

$thefile = $ARGV[0];
print "Massaging $thefile...";

#input record separator;
$irs = $/;
#ouput record separator;
$ors = $\;

open (COMMAVFILE, "<$thefile");

binmode COMMAVFILE;

my @linebuffer;
my %revdateline;
my $inrevs = false;
my @filedump;

# read all of the "version/date/branches/next" lines, removing the date lines:
# 
# 1.2
# date	2000.10.08.17.26.41;	author laine;	state Exp;
# branches;
# next	1.1;
# 

# The last of these lines are followed by a line that says only "desc"

while ($fileline = <COMMAVFILE>) {
    # save the lines to regurgitate later
    push @linebuffer,$fileline;
    if ($fileline =~ /^desc$/) {last;}
    # parse out revision number
    if ($fileline =~ /^([\d]+(\.[\d]+)+)$/) {$rev = $1;}
    elsif ($fileline =~ /^date\s[\d]{2,4}(\.[\d]{2}){5};.*/) {
	# remove the original date line, but keep it in the "dateline
	# array" in case there isn't one in the comments
	$revdateline{$rev} = pop @linebuffer;
    }
}

# Now go through all the "rev/log/comment/text" lines, looking for
# date lines in the comments. When one is found, replace the original
# date line in the array

while ($fileline = <COMMAVFILE>) {
    push @linebuffer,$fileline;
    if ($fileline =~ /^([\d]+(\.[\d]+)+)$/) {$rev = $1;}
    elsif ($fileline =~ /^date\s[\d]{2,4}(\.[\d]{2}){5};.*/) {
	$revdateline{$rev} = $fileline;
	pop @linebuffer;
    }
}

close COMMAVFILE;

open (TEMPFILE, "> tempfile.$$");

binmode TEMPFILE;

# print out all of the "version/date/branches/next" lines, re-adding
# the date line right after the version line.

while ($fileline = shift @linebuffer) {
    print TEMPFILE $fileline;
    if ($fileline =~ /^([\d]+(\.[\d]+)+)$/) {
	$rev = $1;
	print TEMPFILE $revdateline{$rev};
    }
    if ($fileline =~ /^desc$/) {last;}
}

# print all the "rev/log/comment/text" lines.

while ($fileline = shift @linebuffer) {
    print TEMPFILE $fileline;
}

close TEMPFILE;

# move the new file in place of the old one.
system("rm -f \"$thefile\"");
system("mv -f tempfile.$$ \"$thefile\"");
print "OK\n"; 
