#!/usr/bin/perl
# Version: $Id: listshares.pl,v 1.3 2000/11/04 19:58:05 laine Exp $
#

print (STDERR "listshares - List all shared files in a SourceSafe project.\n");
# no buffer
$| = 1;

# Options:
#
# SSROOT=xxx - VSS repository location
#     - default: nothing (uses whatever is already in %ENV{'SSDIR'})
# SSPROJ=xxx - VSS project name
#     - default: none - THIS IS A REQUIRED argument
#     - notes: if this string doesn't start with "$/", one will be appended
# SSUSERPASS=xxx - username,pass for VSS
#     - default: none - uses the current Windows login
# DIRMATCH=regexp - regexp to look for in each directory name. Only display
#                   results for files in directories that contain the regexp
#     - default: none - match all directories
#
# cycle through the commandline and process options
while ($opt = shift)
{
    ($field, $value) = split(/=/, $opt, 2);
    $ENV{uc($field)} = "$value";
}

$ssroot = $ENV{'SSROOT'};
$ssproj = $ENV{'SSPROJ'};
$ssuserpass = $ENV{'SSUSERPASS'};
$dirmatch = $ENV{'DIRMATCH'};
$dirreplace = $ENV{'DIRREPLACE'};

# if $ssroot isn't empty set %ENV{'SSDIR'} to its value
$ENV{'SSDIR'} = backslashes($ssroot) if ($ssroot);

# if $ssproj is empty, print a usage message and quit
die "You *must* specify an SSPROJ!\n"
    unless ($ssproj);

if ($dirreplace) {
    die "DIRMATCH is required if DIRREPLACE is specified!\n"
	unless ($dirmatch);
}
# if $ssproj doesn't start with "$/", prepend it
$ssproj = slashes($ssproj);
$ssproj =~ s/^/\$\// unless ($ssproj =~ /\$\//);

# if $ssuserpass isn't empty, prepend "-y" to it
$ssuserpass =~ s/^/-y/ if ($ssuserpass);

print(STDERR "**********************************************************************\n");
print(STDERR "SSDIR=|$ENV{'SSDIR'}|\n");
print(STDERR "SSPROJ=|$ssproj|\n");
print(STDERR "SSUSERPASS=|$ssuserpass|\n");
print(STDERR "DIRMATCH=|$dirmatch|\n");
print(STDERR "DIRREPLACE=|$dirreplace|\n");
print(STDERR "**********************************************************************\n\n");

# this makes this pattern more useable for matching
$ssprojpat = $ssproj;
$ssprojpat =~ s%\$%\\\$%;
$ssprojpat =~ s%\/%\\/%g;

unlink "lshar_dirs";
unlink "lshar_links";
unlink "lshar_multi";

print(STDERR "Getting Full Listing...");
system("ss dir -R \"$ssproj\" -Olshar_dirs $ssuserpass");
print(STDERR "Ok\n");
open(PROJLIST, "< lshar_dirs");
foreach $fileline (<PROJLIST>) {
    chomp $fileline;
    if ($fileline =~ s/^($ssprojpat[^\:]*)\:/\1/) {
	# This line is the name of a directory, files will follow
	$dirname = $fileline;
	$matched = 1;
	$matched = ($dirname =~ /$dirmatch/) if ($dirmatch);
    } elsif ($matched 
	     && ($fileline !~ /^\$| item\(s\)|^No items found|^[\s]*$/)) {
	# The name of a file in the previously found directory
	print(STDERR "${dirname}/${fileline}...");
	system("ss links \"${dirname}/${fileline}\" $ssuserpass >> lshar_links");
	print(STDERR "Ok\n");
    }
}

close PROJLIST;
unlink "lshar_dirs";

###############
#fudge:

open (LINKSLIST, "< lshar_links");

my @linkbuffer;
my $linkcnt = 0;
my $filename, $linkdir;

open (SHARELIST, "> lshar_multi");

undef @linkbuffer;

while ($fileline = <LINKSLIST>) {
    if ($fileline =~ /^Projects that file \"([^\"]*)\" is in\.\.\.$/) {
	# processing a new file
	if (defined $filename && $linkcnt > 1) {
	    # the current file has more than one share

	    # create a key by sorting and joining all shared directories
	    $sharekey = join(':', sort(@linkbuffer));
	    if ($dirreplace && $dirmatch) {
		$sharekey =~ s/$dirmatch/$dirreplace/g;
	    }
	    # add this file to the end of the list for that key
	    # IFF it's not already in the list
	    $mergedshares{$sharekey} = ":" if (! $mergedshares{$sharekey});
	    if (!($mergedshares{$sharekey} =~ /\:$filename\:/)) {
		$mergedshares{$sharekey} = $mergedshares{$sharekey}.$filename.":";
	    }

	    # the original stuff
	    print SHARELIST "$filename\n";
	    for ($i = 1; $i <= $linkcnt; $i++) {
		# list the shares
		$linkdir = $linkbuffer[$i];
		print SHARELIST "$linkdir\n";
	    }
	}
	$filename = $1;
	$linkcnt = 0;
	undef @linkbuffer;
    } elsif ( ($fileline =~ /^  \$\// ) && ($fileline !~ /   \(/) ) {
	# piling share names into the list
	$linkcnt++;
	chomp $fileline;
	$fileline =~ s/^[\s]*//; # remove leading whitespace
	$linkbuffer[$linkcnt] = $fileline;
    }
}

close SHARELIST;
close LINKSLIST;

foreach $shareddir (sort keys(%mergedshares)) {
    my @files = split(':', $mergedshares{$shareddir});
    my @dirs = split(':', $shareddir);
    print("*********************************************************************\n");
    foreach $dir (@dirs) {
	print("$dir\n") if ($dir);
    }
    foreach $file (sort @files) {
	print("   $file\n") if ($file);
    }
}


sub slashes
{
    my $ret = shift;
    $ret =~ s%\\%\/%g;
    return $ret;
}

sub backslashes
{
    my $ret = shift;
    $ret =~ s%\/%\\%g;
    return $ret;
}


